# GroovyScripts

[Groovy](http://groovy-lang.org) Scripting Engine for [NukkitX](https://github.com/NukkitX/Nukkit)

> **[GroovyLib](https://gitlab.com/PiSystem/GroovyLib) Version: `1.0-2.5.6`**

- [**Download**](build/libs)
- [**Documentation**](DOCS.md)
- [**Examples**](https://gitlab.com/PiSystem/GroovyScriptsExamples/)