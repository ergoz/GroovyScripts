package tech.teslex.gr8scripts.command;

import cn.nukkit.command.Command;
import cn.nukkit.command.CommandSender;
import cn.nukkit.command.ConsoleCommandSender;
import cn.nukkit.command.RemoteConsoleCommandSender;
import cn.nukkit.plugin.Plugin;
import tech.teslex.gr8scripts.script.Gr8Finder;
import tech.teslex.gr8scripts.script.api.Gr8Script;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

public class ScriptCommand extends Command {

	private static final String NAME = "execute";
	private static final String DESCRIPTION = "Run groovy script";
	private static final String USAGE = "/execute <script name> [args]";
	private static final String[] ALIASES = new String[]{"exec"};

	private final Gr8Finder finder;

	private final Plugin plugin;

	public ScriptCommand(Plugin plugin) {
		super(NAME, DESCRIPTION, USAGE, ALIASES);
		this.plugin = plugin;
		this.finder = new Gr8Finder(plugin);
	}


	@Override
	public boolean execute(CommandSender sender, String commandLabel, String[] args) {
		if (sender.hasPermission("gr8.exec")) {

			if (sender instanceof RemoteConsoleCommandSender && !plugin.getConfig().getSection("execute_by_command").getBoolean("allow_by_rcon", false)) {
				sender.sendMessage("Not Allowed");
				return false;
			}

			if (args.length == 0) {
				sender.sendMessage(plugin.getServer().getCommandMap().getCommand("execute").getUsage());
				return false;
			}


			if (sender instanceof ConsoleCommandSender)
				plugin.getLogger().warning("Executing script: §r" + args[0]);
			else
				sender.sendMessage("Executing script: §e" + args[0]);

			if ((args[0].startsWith("http://") || args[0].startsWith("https://")) && plugin.getConfig().getSection("execute_by_command").getBoolean("from_url", false)) {
				Optional<Gr8Script> script = Optional.empty();

				try {
					script = finder.fromNet(args[0]);
				} catch (IOException e) {
					e.printStackTrace();
				}

				System.out.println(script.isPresent());

				script.ifPresent(gr8Script -> evalScript(sender, args, gr8Script));

			} else if (plugin.getConfig().getSection("scripts").getSection("execute_by_command").getBoolean("from_scripts_folder", true)) {
				Optional<Gr8Script> script = finder.findOne(args[0]);

				script.ifPresent(gr8Script -> evalScript(sender, args, gr8Script));
			}
		}

		return false;
	}

	private void evalScript(CommandSender sender, String[] args, Gr8Script script) {
		String[] scriptArgs = Arrays.stream(args).skip(1).toArray(String[]::new);

		Object response;
		try {
			response = script.execute(scriptArgs);
			if (response != null)
				sender.sendMessage("Result: " + response.toString());
		} catch (IOException e) {
			e.printStackTrace();
			sender.sendMessage("Failed to execute " + args[0] + " script!");
		}
	}
}
