package tech.teslex.gr8scripts.utils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class HTTPUtils {

	public static File download(String url, Path to, String fileName) throws IOException {
		URL xUrl = new URL(url);
		URLConnection connection = xUrl.openConnection();
		InputStream is = connection.getInputStream();

		File file = new File(to.toAbsolutePath().toFile().getAbsoluteFile() + File.separator + fileName);

		Files.copy(is, file.toPath(), StandardCopyOption.REPLACE_EXISTING);

		return file;
	}

	public static File download(String url, Path to) throws IOException {
		String fileName = url.substring(url.lastIndexOf('/') + 1);

		return download(url, to, fileName);
	}

	public static String GET(String url) throws IOException {
		URL xUrl = new URL(url);
		URLConnection connection = xUrl.openConnection();

		BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		StringBuilder response = new StringBuilder();

		String line;
		while ((line = bf.readLine()) != null)
			response.append(line);

		return response.toString();
	}

}
