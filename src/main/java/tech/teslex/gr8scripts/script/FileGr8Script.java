package tech.teslex.gr8scripts.script;

import groovy.lang.GroovyShell;
import groovy.lang.Script;
import tech.teslex.gr8scripts.script.api.Gr8Script;

import java.io.File;
import java.io.IOException;

public class FileGr8Script implements Gr8Script {

	private final Script script;
	private final File file;
	private final GroovyShell shell;
	private final ScriptLogger logger;

	private String version = "undefined";
	private String description = "";
	private String usageInfo = "";
	private Boolean autostart = false;
	private Boolean executed = false;


	public FileGr8Script(String version, String description, String usageInfo, Boolean autostart, GroovyShell shell, Script script, File file) {
		this.version = version;
		this.description = description;
		this.usageInfo = usageInfo;
		this.autostart = autostart;
		this.shell = shell;
		this.script = script;
		this.file = file;
		this.logger = new ScriptLogger(this);
	}

	public FileGr8Script(GroovyShell shell, Script script, File file) {
		this.shell = shell;
		this.script = script;
		this.file = file;
		this.logger = new ScriptLogger(this);
	}

	@Override
	public Script getScript() {
		return script;
	}

	public File getFile() {
		return file;
	}

	@Override
	public String getVersion() {
		return version;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String getUsageInfo() {
		return usageInfo;
	}

	@Override
	public Boolean isAutostart() {
		return autostart;
	}

	@Override
	public Boolean isExecuted() {
		return executed;
	}

	@Override
	public String getName() {
		return script.getClass().getName();
	}

	@Override
	public ScriptLogger getLogger() {
		return logger;
	}

	@Override
	public Object execute(String[] args) throws IOException {
		shell.setVariable("args", args);
		shell.setVariable("log", getLogger());
		Object result = shell.evaluate(getFile());
		executed = true;

		return result;
	}
}
