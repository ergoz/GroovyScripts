package tech.teslex.gr8scripts.script.hr;

public enum Gr8HRType {

	SERVER("server"),
	SCRIPT("script");

	private String type;

	Gr8HRType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}
}
