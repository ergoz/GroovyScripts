package tech.teslex.gr8scripts.script.hr;

import cn.nukkit.plugin.Plugin;
import cn.nukkit.utils.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchService;

public class Gr8HotReload {

	private static Logger log;
	private final Plugin plugin;
	private final WatchService watchService;
	private final Path path;


	public Gr8HotReload(Plugin plugin) throws IOException {
		this.plugin = plugin;

		this.watchService = FileSystems.getDefault().newWatchService();
		this.path = new File(plugin.getServer().getDataPath() + File.separator + plugin.getConfig().getString("path", "scripts")).toPath();

		log = plugin.getLogger();
	}

	public void watch(WatchEvent.Kind... eventKinds) throws IOException {
		path.register(watchService, eventKinds);

		plugin.getServer().getScheduler().scheduleAsyncTask(plugin, new Gr8HRTask(plugin, watchService, getType()));
	}

	private Boolean isEnabled() {
		return plugin.getConfig()
				.getSection("dev")
				.getSection("hot-reload")
				.getBoolean("enable", false);
	}

	private Gr8HRType getType() {
		String stringType = plugin.getConfig()
				.getSection("dev")
				.getSection("hot-reload")
				.getString("reload", "server");

		switch (stringType) {
			case "script":
				return Gr8HRType.SCRIPT;
			case "server":
				return Gr8HRType.SERVER;
			default:
				return Gr8HRType.SERVER;
		}
	}
}
