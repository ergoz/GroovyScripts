package tech.teslex.gr8scripts.script;

import cn.nukkit.Server;
import cn.nukkit.utils.LogLevel;
import cn.nukkit.utils.Logger;
import tech.teslex.gr8scripts.script.api.Gr8Script;

public class ScriptLogger implements Logger {

	private final String scriptName;

	public ScriptLogger(Gr8Script context) {
		String prefix = context.getName();
		this.scriptName = "~ [" + prefix + "] ";
	}

	public void emergency(Object message) {
		emergency(message.toString());
	}

	public void alert(Object message) {
		alert(message.toString());
	}

	public void critical(Object message) {
		critical(message.toString());
	}

	public void error(Object message) {
		error(message.toString());
	}

	public void warning(Object message) {
		warning(message.toString());
	}

	public void notice(Object message) {
		notice(message.toString());
	}

	public void info(Object message) {
		info(message.toString());
	}

	public void debug(Object message) {
		debug(message.toString());
	}

	@Override
	public void emergency(String message) {
		this.log(LogLevel.EMERGENCY, message);
	}

	@Override
	public void alert(String message) {
		this.log(LogLevel.ALERT, message);
	}

	@Override
	public void critical(String message) {
		this.log(LogLevel.CRITICAL, message);
	}

	@Override
	public void error(String message) {
		this.log(LogLevel.ERROR, message);
	}

	@Override
	public void warning(String message) {
		this.log(LogLevel.WARNING, message);
	}

	@Override
	public void notice(String message) {
		this.log(LogLevel.NOTICE, message);
	}

	@Override
	public void info(String message) {
		this.log(LogLevel.INFO, message);
	}

	@Override
	public void debug(String message) {
		this.log(LogLevel.DEBUG, message);
	}

	public void log(LogLevel level, Object message) {
		log(level, message.toString());
	}

	@Override
	public void log(LogLevel level, String message) {
		Server.getInstance().getLogger().log(level, this.scriptName + message);
	}


	public void emergency(Object message, Throwable t) {
		emergency(message.toString(), t);
	}

	public void alert(Object message, Throwable t) {
		alert(message.toString(), t);
	}

	public void critical(Object message, Throwable t) {
		critical(message.toString(), t);
	}

	public void error(Object message, Throwable t) {
		error(message.toString(), t);
	}

	public void warning(Object message, Throwable t) {
		warning(message.toString(), t);
	}

	public void notice(Object message, Throwable t) {
		notice(message.toString(), t);
	}

	public void debug(Object message, Throwable t) {
		debug(message.toString(), t);
	}

	@Override
	public void emergency(String message, Throwable t) {
		this.log(LogLevel.EMERGENCY, message, t);
	}

	@Override
	public void alert(String message, Throwable t) {
		this.log(LogLevel.ALERT, message, t);
	}

	@Override
	public void critical(String message, Throwable t) {
		this.log(LogLevel.CRITICAL, message, t);
	}

	@Override
	public void error(String message, Throwable t) {
		this.log(LogLevel.ERROR, message, t);
	}

	@Override
	public void warning(String message, Throwable t) {
		this.log(LogLevel.WARNING, message, t);
	}

	@Override
	public void notice(String message, Throwable t) {
		this.log(LogLevel.NOTICE, message, t);
	}

	@Override
	public void info(String message, Throwable t) {
		this.log(LogLevel.INFO, message, t);
	}

	@Override
	public void debug(String message, Throwable t) {
		this.log(LogLevel.DEBUG, message, t);
	}


	public void log(LogLevel level, Object message, Throwable t) {
		log(level, message.toString(), t);
	}

	@Override
	public void log(LogLevel level, String message, Throwable t) {
		Server.getInstance().getLogger().log(level, this.scriptName + message, t);
	}
}
