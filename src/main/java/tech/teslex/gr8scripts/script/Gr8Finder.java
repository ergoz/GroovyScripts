package tech.teslex.gr8scripts.script;

import cn.nukkit.plugin.Plugin;
import cn.nukkit.utils.ConfigSection;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;
import groovy.transform.CompileStatic;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ASTTransformationCustomizer;
import org.codehaus.groovy.control.customizers.ImportCustomizer;
import org.codehaus.groovy.runtime.metaclass.MissingPropertyExceptionNoStack;
import tech.teslex.gr8scripts.registrator.GroovyNukkitCommandRegister;
import tech.teslex.gr8scripts.registrator.GroovyNukkitEventHandlerRegister;
import tech.teslex.gr8scripts.script.api.Gr8Script;
import tech.teslex.gr8scripts.utils.HTTPUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Gr8Finder {

	private final Plugin plugin;

	public Gr8Finder(Plugin plugin) {
		this.plugin = plugin;
	}

	public Optional<Gr8Script> fromNet(String url) throws IOException {
		File tmpFolder = new File(plugin.getServer().toString() + File.separator + ".scripts-net-tmp");
		tmpFolder.mkdirs();


		File downloadedFile = HTTPUtils.download(url, tmpFolder.toPath());

		return Optional.ofNullable(mapFileToGr8Scrip().apply(downloadedFile));
	}

	public List<Gr8Script> findAll() {
		File scriptsPath = getScriptsPath().toFile();
		File[] files = scriptsPath.listFiles(file -> file.getName().endsWith(".groovy"));

		List<Gr8Script> scripts = new ArrayList<>();
		if (files != null) {
			scripts = Arrays.stream(files).map(mapFileToGr8Scrip()).collect(Collectors.toList());
		}

		return scripts;
	}

	public Optional<Gr8Script> findOne(String name) {
		if (!name.endsWith(".groovy"))
			name += ".groovy";

		File scriptsPath = getScriptsPath().toFile();
		String finalName = name;
		File[] files = scriptsPath.listFiles(file -> finalName.equals(file.getName()));

		Gr8Script script = null;
		if (files != null && files.length != 0) {
			script = mapFileToGr8Scrip().apply(files[0]);
		}

		return Optional.ofNullable(script);
	}

	private Function<? super File, Gr8Script> mapFileToGr8Scrip() {
		return file -> {
			try {
				Binding binding = new Binding();
				binding.setVariable("server", plugin.getServer());
				binding.setVariable("events", new GroovyNukkitEventHandlerRegister(plugin));
				binding.setVariable("commands", new GroovyNukkitCommandRegister(plugin));
				binding.setVariable("plugin", plugin);

				ImportCustomizer defaultImports = new ImportCustomizer();

				String[] packagesImports = getPackagesImports().toArray(new String[0]);
				String[] classesImports = getClassesImports().toArray(new String[0]);
				String[] staticImports = getStaticImports().toArray(new String[0]);

				defaultImports.addStarImports(packagesImports);
				defaultImports.addImports(classesImports);
				defaultImports.addStaticStars(staticImports);
				getMapImports().forEach((k, v) -> defaultImports.addImport(k, String.valueOf(v.toString())));

				CompilerConfiguration config = new CompilerConfiguration();
				config.addCompilationCustomizers(defaultImports);

				if (isCompileStatic())
					config.addCompilationCustomizers(new ASTTransformationCustomizer(CompileStatic.class));

				GroovyShell shell = new GroovyShell(plugin.getClass().getClassLoader(), binding, config);

				Script script = shell.parse(file);

				Boolean autostart = false;
				String version = "undefined";
				String description = "";
				String usageInfo = "";

				try {
					Object metaObj = script.getProperty("meta");
					if (metaObj instanceof Map) {
						Map<String, Object> meta = (Map) metaObj;

						autostart = (Boolean) meta.getOrDefault("autostart", false);
						version = (String) meta.getOrDefault("version", "undefined");
						description = (String) meta.getOrDefault("description", "");
						usageInfo = (String) meta.getOrDefault("usageInfo", "");
					}
				} catch (MissingPropertyExceptionNoStack exceptionNoStack) {
					// just ignore it
				}

				return new FileGr8Script(version, description, usageInfo, autostart, shell, script, file);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException("Failed to map script.");
			}
		};
	}

	private Path getScriptsPath() {
		String configPath = plugin.getConfig().get("path", "scripts");
		File file = new File(plugin.getServer().getDataPath() + File.separator + configPath);
		file.mkdirs();
		return file.toPath();
	}

	private List<String> getPackagesImports() {
		Optional<List<String>> stringList = Optional.ofNullable(plugin.getConfig().getSection("imports").getStringList("packages"));
		return stringList.orElse(Collections.emptyList());
	}

	private List<String> getClassesImports() {
		Optional<List<String>> stringList = Optional.ofNullable(plugin.getConfig().getSection("imports").getStringList("classes"));
		return stringList.orElse(Collections.singletonList("groovy.transform.Field"));
	}

	private List<String> getStaticImports() {
		Optional<List<String>> stringList = Optional.ofNullable(plugin.getConfig().getSection("imports").getStringList("static"));
		return stringList.orElse(Collections.emptyList());
	}

	private Map<String, Object> getMapImports() {
		Optional<ConfigSection> imports = Optional.ofNullable(plugin.getConfig().getSection("imports").getSection("class-aliases"));
		return imports.orElse(new ConfigSection());
	}

	private Boolean isCompileStatic() {
		return plugin.getConfig().getBoolean("compile-static", false);
	}
}
