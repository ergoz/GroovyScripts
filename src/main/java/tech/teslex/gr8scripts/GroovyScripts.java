package tech.teslex.gr8scripts;

import cn.nukkit.plugin.PluginBase;
import tech.teslex.gr8scripts.command.ScriptCommand;
import tech.teslex.gr8scripts.script.Gr8Finder;
import tech.teslex.gr8scripts.script.api.Gr8Script;
import tech.teslex.gr8scripts.script.hr.Gr8HotReload;
import tech.teslex.gr8scripts.utils.HTTPUtils;

import java.io.IOException;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.util.*;
import java.util.stream.Collectors;

public class GroovyScripts extends PluginBase {

	public static final String version = "1.1";

	public static final String groovyLibUrl = "https://gitlab.com/PiSystem/GroovyLib";

	private List<Runnable> onPluginDisableHandlers = new ArrayList<>();

	public void handleDisable(Runnable runnable) {
		onPluginDisableHandlers.add(runnable);
	}

	@Override
	public void onDisable() {
		onPluginDisableHandlers.forEach(Runnable::run);
	}


	@Override
	public void onLoad() {
		saveResource("config.yml");
	}

	@Override
	public void onEnable() {
		saveConfig();


		if (getConfig().getBoolean("checking_updates")) {
			try {
				getLogger().info("Checking updates..");
				checkUpdate();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (getServer().getPluginManager().getPlugin("GroovyLib") == null) {
			getLogger().error("Install GroovyLib!");
			getLogger().error("GroovyLib: §r" + groovyLibUrl);

			getServer().getPluginManager().disablePlugin(this);
		}

		if (getConfig().getSection("dev").getBoolean("enable", false)) {
			try {
				Gr8HotReload hotReload = new Gr8HotReload(this);

				WatchEvent.Kind[] kinds = getEventsKinds().toArray(new WatchEvent.Kind[0]);

				getLogger().notice("Hot-reload: " + Arrays.toString(kinds));
				hotReload.watch(kinds);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}


		if (isScriptsEnabled()) {
			getServer().getCommandMap().register("execute", new ScriptCommand(this));
			Gr8Finder finder = new Gr8Finder(this);

			List<Gr8Script> scriptsFromFolder = finder.findAll();

			if (isScriptsAutoExecute())
				executeAll(scriptsFromFolder);
		}
	}

	private void executeAll(List<Gr8Script> scripts) {
		scripts
				.stream()
				.filter(Gr8Script::isAutostart)
				.forEach(this::executeScript);
	}

	private Object executeScript(Gr8Script script) {
		try {
			if (isLogOnExecuteScript())
				getLogger().warning("Executing script: §r" + script.getName());
			return script.execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new Object();
	}

	private void checkUpdate() throws IOException {
		String v = HTTPUtils.GET("https://gitlab.com/PiSystem/GroovyScripts/raw/master/VERSION");

		if (!v.equals(version))
			getLogger().warning("New version found: §ev" + v);
	}

	private boolean isLogOnExecuteScript() {
		return getConfig().getSection("log").getBoolean("on_execute_script", true);
	}

	private boolean isScriptsAutoExecute() {
		return getConfig().getBoolean("autoexecute", false);
	}

	private boolean isScriptsEnabled() {
		return getConfig().getBoolean("enabled", false);
	}

	private Set<WatchEvent.Kind> getEventsKinds() {
		Optional<List<String>> events = Optional.ofNullable(getConfig().getSection("dev").getSection("hot-reload").getStringList("events"));

		return events.<Set<WatchEvent.Kind>>map(strings -> strings.stream()
				.map(e -> {
					switch (e) {
						case "create":
							return StandardWatchEventKinds.ENTRY_CREATE;
						case "modify":
							return StandardWatchEventKinds.ENTRY_MODIFY;
						case "delete":
							return StandardWatchEventKinds.ENTRY_DELETE;
						default:
							return StandardWatchEventKinds.OVERFLOW;
					}
				})
				.collect(Collectors.toSet()))
				.orElse(Collections.emptySet());
	}
}
